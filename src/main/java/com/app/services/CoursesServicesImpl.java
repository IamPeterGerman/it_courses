package com.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.cours.Courses;
import com.app.cours.CoursesDAO;
import com.app.dto.CoursesDTO;

@Service
public class CoursesServicesImpl implements CoursesServices {
	
	@Autowired
	private CoursesDAO coursesDAO;

	@Override
	public void saveCourses(CoursesDTO course) {
		
		coursesDAO.addNewCours(course.getCours(), course.getTeacher_id());
	}

	@Override
	public void updateCourses(CoursesDTO course) {
		
		coursesDAO.updateCours(course.getId(), course.getCours(), course.getTeacher_id());
	}

	@Override
	public List<Courses> getAllCoursesList() {
		
		return coursesDAO.findAll();
	}

	@Override
	public CoursesDTO getCourseById(Long id) {
		
		Courses courses = coursesDAO.getOne(id);
		CoursesDTO cours = new CoursesDTO();
		
		cours.setId(courses.getId());
		cours.setCours(courses.getCours());
		cours.setTeacher_id(courses.getTeachers().getId());
		
		return cours;
	}

	@Override
	public void deleteCourseById(Long id) {
		
		coursesDAO.deleteById(id);
	}

}
