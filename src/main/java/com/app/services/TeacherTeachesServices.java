package com.app.services;

import java.util.List;

import com.app.dto.TeacherTeachesDTO;

public interface TeacherTeachesServices {
	
	List<TeacherTeachesDTO> showCoursesByTeacher(String firstname, String lastname);
}
