package com.app.services;

import java.util.List;

import com.app.cours.Courses;
import com.app.dto.CoursesDTO;

public interface CoursesServices {
	
	void saveCourses(CoursesDTO course);
	
	void updateCourses(CoursesDTO course);
	
	List<Courses> getAllCoursesList();
	
	CoursesDTO getCourseById(Long id);
	
	void deleteCourseById(Long id);
}
