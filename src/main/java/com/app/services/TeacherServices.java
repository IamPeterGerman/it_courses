package com.app.services;

import java.util.List;

import com.app.dto.TeachersDTO;
import com.app.teacher.Teachers;

public interface TeacherServices {

	void saveTeacher(TeachersDTO teacher);
	
	void updateTeacher(TeachersDTO teacher);
	
	List<Teachers> getAllTeachers();
	
	TeachersDTO getTeacherById(Long id);
	
	void deleteTeacherById(Long id);
}
