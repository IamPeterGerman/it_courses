package com.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dto.TeachersDTO;
import com.app.teacher.Teachers;
import com.app.teacher.TeachersDAO;

@Service
public class TeacherServicesImpl implements TeacherServices {

	@Autowired
	private TeachersDAO teachersDAO;
	
	@Override
	public void saveTeacher(TeachersDTO teacher) {
		
		teachersDAO.addNewTeacher(teacher.getFirstName(), teacher.getLastName());
	}

	@Override
	public void updateTeacher(TeachersDTO teacher) {
		
		teachersDAO.updateTeacher(teacher.getId(), teacher.getFirstName(), teacher.getLastName());
	}

	@Override
	public List<Teachers> getAllTeachers() {
		
		return teachersDAO.findAll();
	}

	@Override
	public TeachersDTO getTeacherById(Long id) {
		
		Teachers teachers = teachersDAO.getOne(id);
		TeachersDTO teacher = new TeachersDTO();
		
		teacher.setId(teachers.getId());
		teacher.setFirstName(teachers.getFirst_name());
		teacher.setLastName(teachers.getLast_name());
		
		return teacher;
	}

	@Override
	public void deleteTeacherById(Long id) {
		
		teachersDAO.deleteById(id);
	}

}
