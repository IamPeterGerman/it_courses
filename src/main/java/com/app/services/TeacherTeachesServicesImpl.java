package com.app.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.cours.Courses;
import com.app.cours.CoursesDAO;
import com.app.dto.TeacherTeachesDTO;
import com.app.teacher.TeachersDAO;

@Service
public class TeacherTeachesServicesImpl implements TeacherTeachesServices {
		
	@Autowired
	private TeachersDAO teacherDAO;
	
	@Autowired
	private CoursesDAO coursesDAO;

	@Override
	public List<TeacherTeachesDTO> showCoursesByTeacher(String firstname, String lastname) {
		
		// вычисляем id учителя
		Long id = teacherDAO.findIdByName(firstname, lastname);
		
		// находим курсы которые преподаёт учитель - у которых мы выше вычисляли id
		List<Courses> courses = coursesDAO.returnCoursesByTeacher(id);
		
		// список который будем возвращать клиэнту
		List<TeacherTeachesDTO> teacherTeachesDTO = new ArrayList<>();
		
			for(Courses cours: courses) {
				TeacherTeachesDTO teaches = new TeacherTeachesDTO();
				
				teaches.setNameTeacher(firstname);
				teaches.setLastNameTeacher(lastname);
				teaches.setCours(cours.getCours());
				
				teacherTeachesDTO.add(teaches);
			}

		return teacherTeachesDTO;
	}
	
}
