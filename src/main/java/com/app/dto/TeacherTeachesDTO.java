package com.app.dto;

public class TeacherTeachesDTO {

	private String nameTeacher;
	private String lastNameTeacher;
	private String cours;
	
	public TeacherTeachesDTO() {}

	public TeacherTeachesDTO(String nameTeacher, String lastNameTeacher, String cours) {
		super();
		this.nameTeacher = nameTeacher;
		this.lastNameTeacher = lastNameTeacher;
		this.cours = cours;
	}

	public String getNameTeacher() {
		return nameTeacher;
	}

	public void setNameTeacher(String nameTeacher) {
		this.nameTeacher = nameTeacher;
	}

	public String getLastNameTeacher() {
		return lastNameTeacher;
	}

	public void setLastNameTeacher(String lastNameTeacher) {
		this.lastNameTeacher = lastNameTeacher;
	}

	public String getCours() {
		return cours;
	}

	public void setCours(String cours) {
		this.cours = cours;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cours == null) ? 0 : cours.hashCode());
		result = prime * result + ((lastNameTeacher == null) ? 0 : lastNameTeacher.hashCode());
		result = prime * result + ((nameTeacher == null) ? 0 : nameTeacher.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeacherTeachesDTO other = (TeacherTeachesDTO) obj;
		if (cours == null) {
			if (other.cours != null)
				return false;
		} else if (!cours.equals(other.cours))
			return false;
		if (lastNameTeacher == null) {
			if (other.lastNameTeacher != null)
				return false;
		} else if (!lastNameTeacher.equals(other.lastNameTeacher))
			return false;
		if (nameTeacher == null) {
			if (other.nameTeacher != null)
				return false;
		} else if (!nameTeacher.equals(other.nameTeacher))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TeacherTeachesDTO [nameTeacher=" + nameTeacher + ", lastNameTeacher=" + lastNameTeacher + ", cours="
				+ cours + "]";
	}
	
	
}
