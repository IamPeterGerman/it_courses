package com.app.cours;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CoursesDAO extends JpaRepository<Courses, Long> {

	@Query("SELECT c FROM Courses c WHERE c.teachers.id = :teachers_id")
	List<Courses> returnCoursesByTeacher(@Param("teachers_id") Long teachers_id);
	
	@Query(value = "INSERT INTO courses(cours, teachers_id)"
			+ " VALUES(?1, ?2)", nativeQuery=true)
	void addNewCours(String cours, Long teachers_id);
	
	@Query(value = "UPDATE courses SET cours=?2, teachers_id=?3 WHERE id=?1", nativeQuery=true)
	void updateCours(Long id, String cours, Long teacher_id);
}
