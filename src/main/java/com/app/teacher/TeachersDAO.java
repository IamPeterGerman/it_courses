package com.app.teacher;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TeachersDAO extends JpaRepository<Teachers, Long>{

	@Query("SELECT v.id FROM Teachers v WHERE v.first_name = :firstname AND v.last_name = :lastname")
	Long findIdByName(@Param("firstname") String firstname, @Param("lastname")String lastname);
	
	@Query(value = "INSERT INTO teachers(first_name, last_name)"
			+ " VALUES(?1, ?2)", nativeQuery=true)
	void addNewTeacher(String firstName, String lastName);
	
	@Query(value = "UPDATE teachers SET first_name=?2, last_name=?3 WHERE id=?1", nativeQuery=true)
	void updateTeacher(Long id, String firstName, String lastName);
}
