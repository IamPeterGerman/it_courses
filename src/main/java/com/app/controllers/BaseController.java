package com.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.TeacherTeachesDTO;
import com.app.services.TeacherTeachesServicesImpl;

@RestController
public class BaseController {
	
	@Autowired
	private TeacherTeachesServicesImpl teacherTeaches;

	@GetMapping("/using/{firstname}/{lastname}")
	List<TeacherTeachesDTO> getCoursesOfTicher(@PathVariable String firstname, @PathVariable String lastname) {
		
		return teacherTeaches.showCoursesByTeacher(firstname, lastname);
	}
}
