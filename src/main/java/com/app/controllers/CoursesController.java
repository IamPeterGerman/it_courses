package com.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.cours.Courses;
import com.app.dto.CoursesDTO;
import com.app.services.CoursesServices;

@RestController
public class CoursesController {

	@Autowired
	private CoursesServices coursesServices;
	
	@PostMapping("/save_cours")
	public void save(@RequestBody CoursesDTO course) {
		
		coursesServices.saveCourses(course);
	}
	
	@PutMapping("/update_cours")
	public void update(@RequestBody CoursesDTO course) {
		
		coursesServices.updateCourses(course);
	}
	
	@GetMapping("/all_courses")
	public List<Courses> getAllCourses() {
		
		return coursesServices.getAllCoursesList();
	}
	
	@GetMapping("/show_cours/{id}")
	public CoursesDTO getCourses(@PathVariable Long id) {
		
		return coursesServices.getCourseById(id);
	}
	
	@DeleteMapping("/delete_cours/{id}")
	public void deleteCours(@PathVariable Long id) {
		
		coursesServices.deleteCourseById(id);
	}
}
