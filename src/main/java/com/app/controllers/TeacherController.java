package com.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.TeachersDTO;
import com.app.services.TeacherServices;
import com.app.teacher.Teachers;

@RestController
public class TeacherController {

	@Autowired
	private TeacherServices teacherServices;
	
	@PostMapping("/save_teacher")
	public void save(@RequestBody TeachersDTO teacher) {
		
		teacherServices.saveTeacher(teacher);
	}
	
	@PutMapping("/update_teacher")
	public void update(@RequestBody TeachersDTO teacher) {
		
		teacherServices.updateTeacher(teacher);
	}
	
	@GetMapping("/all_teachers")
	public List<Teachers> getAllTeacher() {
		
		return teacherServices.getAllTeachers();
	}
	
	@GetMapping("/get_teacher/{id}")
	public TeachersDTO getTeacher(@PathVariable Long id) {
		
		return teacherServices.getTeacherById(id);
	}
	
	@DeleteMapping("/delete_teacher/{id}")
	public void deleteTeacher(@PathVariable Long id) {
		
		teacherServices.deleteTeacherById(id);
	}
}
